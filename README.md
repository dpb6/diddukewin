# Did Duke Win?

#### An IoT project using the SparkFun Inventor's Kit for Photon

## Project abstract

If you missed the last Duke Men's basketball game, use this IoT device to instantly check the outcome and score via a button press, voice command, or other IFTTT trigger! This project uses an Http Get Request, OLED screen, servomotor and interrupts.

## Full project description

I built this project to utilize some of the coolest components of the SparkFun Inventor's Kit: the MicroOLED screen, the servomotor, and the internet-connected P1 Photon chip. A library is used to make a Get Request to pull information from [diddukewin.com](http://www.diddukewin.com). Particle Cloud functions are used to expose the core functionality of the device to the web.

### Parts Used

- SparkFun Photon RedBoard
- Mini breadboard
- SparkFun Micro OLED
- Push-button
- Servomotor
- Hookup jumper wires
- Micro-USB power source (V-USB used for servo)

### Source Code

The `diddukewin.ino` file is included in the `src` folder of the Gitlab repo: [https://gitlab.oit.duke.edu/dpb6/diddukewin](https://gitlab.oit.duke.edu/dpb6/diddukewin)

If you are using the [Build IDE](https://build.particle.io), you need to add the `SparkFunMicroOLED` library (version 1.3.0) to your project and create new files for the included `HttpClient.cpp` and `HttpClient.h`, which were modified to enlarge the buffer size for the response body.

Techniques and Features Utilized:

- HttpClient library to make a Get request
- OLED library to write to the screen
- Servo object for waving the Duke flag
- Software Interrupt to catch the button press
- Particle Cloud functions to expose the `wave`, `display` and `check` routines
- Particle Publish commands for score and game outcome

### IFTTT voice command

Using an [IFTTT Applet](https://ifttt.com/applets/74196413d-if-you-say-did-duke-win-then-call-a-function) I connected my Google Assistant with my Particle Photon Cloud Function `check()`. I can now say, "Ok Google, Did Duke Win?" and my IoT device will check the web and show me the result.

### Hardware wiring

![Wiring Diagram with Photon RedBoard, OLED, servo, and pushbutton](img/fritzing_duke_iot_bb.png)

(Fritzing file included in the `img` directory.)

### Photo

![Photo of project including homemade flag](img/diddukewin.jpg)
