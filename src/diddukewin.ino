// This #include statement was automatically added by the Particle IDE.
#include <SparkFunMicroOLED.h>
#include "HttpClient.h"
#include "math.h"

//////////////////////////
// Button Pins          //
//////////////////////////
#define BLUE_BUTTON D4

//////////////////////////
// MicroOLED Definition //
//////////////////////////
#define PIN_OLED_RST D6 // Connect RST to pin 6
#define PIN_OLED_DC  D5 // Connect DC to pin 5 (required for SPI)
#define PIN_OLED_CS  A2 // Connect CS to pin A2 (required for SPI)
MicroOLED oled(MODE_SPI, PIN_OLED_RST, PIN_OLED_DC, PIN_OLED_CS);

//////////////////////////
// Timing               //
//////////////////////////
unsigned int nextTime = millis() + 1000; // Next time to contact the server

//////////////////////////
// HttpClient library   //
//////////////////////////
HttpClient http;
// Headers currently need to be set at init, useful for API keys etc.
http_header_t headers[] = {
    //  { "Content-Type", "application/json" },
    //  { "Accept" , "application/json" },
    { "Accept" , "*/*"},
    { "User-agent", "Particle HttpClient"},
    { NULL, NULL } // NOTE: Always terminate headers will NULL
};
http_request_t request;
http_response_t response;

//////////////////////////
// Servomotor setup     //
//////////////////////////
Servo myservo;            // create servo object to control a servo
                          // a maximum of eight servo objects can be created

//////////////////////////
// flag for check() ISR //
//////////////////////////
int checkFlag = 0;

//////////////////////////
// Particle Variables   //
//////////////////////////
String score;
String wl;
int pos; // variable to store the servo position

void setup() {
    // Request path and body can be set at runtime or at setup.
    request.hostname = "www.diddukewin.com";
    request.port = 80;
    request.path = "/";
    // The library also supports sending a body with your request:
    //request.body = "{\"key\":\"value\"}";

    pinMode(BLUE_BUTTON, INPUT_PULLUP); // Setup the MODE_PIN as an input, with pull-up.
    // Since it has a pull-up, the MODE_PIN will be LOW when the button is pressed.
    // Attach an interrupt to BLUE_BUTTON, `check` will be the name of the ISR,
    // setting the event to RISING means the ISR will be triggered when the button is released
    attachInterrupt(BLUE_BUTTON, setFlag, RISING);
    // Call the interrupts() function to enable interrupts.
    // The opposite of interrupts() is noInterrupts(), which disables external interrupts.
    interrupts();

    oled.begin(); // Initialize the OLED

    myservo.attach(D0);  // attaches the servo on the D0 pin to the servo object
    pos = myservo.read(); // variable to store the servo position
    if (pos < 0) {
        pos = 75;
    }

    // register the cloud functions
    Particle.function("check", check);
    Particle.function("display", display);
    Particle.function("wave", wave);
    Particle.function("move", move);
    Particle.function("moveTo", moveTo);

    // reserve space for strings to be written later
    score.reserve(10);
    wl.reserve(4);

    // register the cloud variables
    // variable name max length is 12 characters long
    Particle.variable("score", score);
    Particle.variable("wl", wl);
    Particle.variable("pos", pos);
}

void loop() {
    if (nextTime > millis()) {
        return;
    }
    
    // if flag was set by ISR button press
    if (checkFlag == 1) {
        // disable interrupts
        noInterrupts();
        // run the slow check routine
        int res = check("");
        // set the time for the next loop iteration
        nextTime = millis() + 1000;
        // reset flag
        checkFlag = 0;
        // enable interrupts
        interrupts();
    }
}

//////////////////////////
// ISR handler routine  //
//  set check() flag    //
//////////////////////////
void setFlag() {
    // just set global flag
    checkFlag = 1;
}

//////////////////////////
// Particle Cloud Funct //
//  check game result   //
//////////////////////////
int check(String command) {
    // Clear the OLED so we know things are changing
    oled.clear(PAGE);
    
    // Get request
    http.get(request, response, headers);

    if (response.status == 200){
        String string =  String(response.body);
        // Excerpt: 
        // <p class='yes'>YES</p><p><a href='http://espn.go.com/ncb/recap?gameId=400986293&version=mobile&teamId=150'>W 74-64</a></p>
        
        int scoreEnd = string.lastIndexOf("</a></p>");
        int scoreStart = string.lastIndexOf(">",scoreEnd)+1;
        if (scoreEnd != -1 && scoreStart != -1) {
            score = string.substring(scoreStart, scoreEnd);
            Particle.publish("score", score, 60, PRIVATE);
            int res = display("DidDukeWin? "+score);
        }
        else {
            return -1; // -1 error code means score not found
        }
        
        int wlStart = string.indexOf("'>", string.indexOf("p class")) + 2;
        int wlEnd = string.indexOf("<",wlStart);
        if (wlStart != -1 && wlEnd != -1) {
            wl = string.substring(wlStart, wlEnd);
            Particle.publish("winloss", wl, 60, PRIVATE);
            int res = wave(wl);
        }
        else {
            return -2; // -2 error code means wl not found
        }
    }
    else {
        return -3; // bad response status
    }
}

//////////////////////////
// Particle Cloud Funct //
//  print to OLED       //
//////////////////////////
int display(String string) {
    oled.clear(PAGE);     // Clear the display
    oled.setFontType(1);  // Change the font to medium-size
    oled.setCursor(0, 0); // Set the cursor to top-left
    oled.print(string);   // Write string to buffer
    oled.display();       // Update the display
    return 0;
}

//////////////////////////
// Particle Cloud Funct //
//  move the servo      //
//////////////////////////
int move(String command) {
    if (command == "UP") {
        int res = moveTo("162");
    }
    else if (command == "DOWN") {
        int res = moveTo("21");
    }
    else if (command == "HALF") {
        int res = moveTo("75");
    }
    else {
        return -1; // -1 for invalid command
    }
    return 0; // 0 for success
}

//////////////////////////
// Particle Cloud Funct //
//  move the servo to N //
//////////////////////////
int moveTo(String command) {
    int newPos = command.toInt();
    
    // Move it up if it is below
    for(; pos<newPos; pos++) {
        myservo.write(pos); // tell servo to go to position in variable 'pos'
        delay(15);          // waits 15ms for the servo to reach the position
    }
    // Move it down if it is above
    for(; pos>newPos; pos--) {
        myservo.write(pos); // tell servo to go to position in variable 'pos'
        delay(15);          // waits 15ms for the servo to reach the position
    }
    return 0; // 0 for success
}

//////////////////////////
// Particle Cloud Funct //
//  wave the flag servo //
//////////////////////////
int wave(String command) {
    if (command == "YES") {     // wave the flag
        int res = move("UP");
        res = res + move("DOWN");
        res = res + move("HALF");
        if (res >= 0) {
            return 1;           // 1 for YES
        }
        else {
            return -1;
        }
    }
    else if (command == "NO") { // put the flag down
        int res = move("DOWN");
        if (res >= 0) {
            return 0;           // 0 for NO
        }
        else {
            return -1;
        }
    }
    else {
        return -1;              // -1 for bad command
    }
}
